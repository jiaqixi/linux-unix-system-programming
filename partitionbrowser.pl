#!/usr/bin/perl


#check if it is file

if(-e $ARGV[0])
{
 # print "File $ARGV[0] exists!\n";

# check if it is readable
  if(-r $ARGV[0])
  { 
    # print"File $ARGV[0] is readable!\n";
     open(FILE1, $ARGV[0])||die"cannot open the file $!\n";
     
# print the total number of partitions in partition_file
    @word = <FILE1>;
#count is used to calculate the number of partitions
     $length=@ARGV;
    if($length==1)
     {
      $count=0;
      foreach $a(@word)
      {
        if($a =~ /\/\w{1,}\/\w{1,}/)
         { 
             $count++;
          }
       }
       
       if($count == 0)
       {
          print"No partitions found in argument file\n";
       } 
       else
       {
         print "Total number of partitions: $count \n"; 
       }
    }


# use -f to print file system type
    
     
     elsif($ARGV[1] =~ /^-f$/)
      { $m=0;
        @arr=(0);
        @arr2=(0);
          foreach $b(@word)
          {
              @arr = split(/ /, $b);
              if($arr[2] =~ m/\w{1,}/)
              {
                $arr2[$m]=$arr[2];
                $m++;
              }                 
          }

         if($m==0)
         {print "No file systems found in argument file\n";}
         else
         { 
            # %count;
             @uniq = grep {!$count{$_}++ } @arr2;
             print "File systems in use: @uniq\n";
        
         }
          
         
      }
      elsif($ARGV[1] =~ /^-w$/)
      {  $m=0;
         foreach $b(@word)
         {
           @arr = split(/ /, $b);
           if($arr[3]=~ /^rw$/)
             {$m++;} 
         }
        print "Number of read-write partition : $m \n";
      }
      elsif($ARGV[1] =~ /^-a$/)
      { $sum=0;
        foreach $b(@word)
        {
           @arr = split(/ /, $b);
           if($arr[3]=~ /^rw$/)
            {
              $sum=$sum+ $arr[4]-$arr[5];
             
            }
        }
           
       print"Overall space available in read-write partitions: $sum\n";
      }
      elsif($ARGV[1] =~ /^-p$/)
      { 
        foreach $b(@word)
        {
         
          if($b =~ /$ARGV[2]/)
          { 
             @arr = split(/ /,$b);
             $available= $arr[4]-$arr[5];
             if($arr[3]=~ /^rw$/)
              {
                 print"Partition $arr[0] is read-write and has $available available space\n ";
              }
             elsif($arr[3]=~ /^ro$/)
              {
                 print "Partition $arr[0] is read-only and has $available availab space\n";
              }
            
          }

        } 
      }
      elsif($ARGV[1] =~ /^-s$/)
      {
        print "Name:Jiaqi\nSurname: Xi\nStudent number: 11611148\nDate of completion of assignment : 28th October 2014\n";  
      }
      else
      {
        print "It is incorrect, please re-enter argument\n";
      }
  }else
   {
       print "File $ARGV[0] is not readable!\n";
    }
}else
{
  print"File $ARGV[0] does not exist!\n";
}
